package com.example.library.service;

import com.example.library.dto.AuthorDto;
import com.example.library.entity.Author;
import com.example.library.entity.Book;

import java.util.List;

public interface IAuthorService {
    Author create(AuthorDto authorDto);

    List<Author> fetchAll();

    Author getAuthorById(Long authorId);

    Author update(AuthorDto authorDto, Long authorId);

    void delete(Long authorId);
}
