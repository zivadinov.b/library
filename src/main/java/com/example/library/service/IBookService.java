package com.example.library.service;

import com.example.library.dto.BookDto;
import com.example.library.entity.Book;

import java.util.List;

public interface IBookService {
    Book create(BookDto bookDto);

    List<Book> fetchAll();

    Book getBookById(Long bookId);

    Book update(BookDto bookDto, Long bookId);

    void delete(Long bookId);
}
