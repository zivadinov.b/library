package com.example.library.service.impl;

import com.example.library.entity.User;
import com.example.library.repository.UserRepository;
import com.example.library.util.JwtUtil;
import com.example.library.util.exception.AuthenticationException;
import com.example.library.util.exception.ErrorMessageConstants;
import com.example.library.util.exception.RegistrationException;
import com.example.library.util.model.AuthenticationRequest;
import com.example.library.util.model.TokenResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import static com.example.library.util.exception.ErrorMessageConstants.*;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtTokenProvider;

    public TokenResponseDto authenticate(AuthenticationRequest authenticationRequestDto) {
        User userEntity = userRepository.findByUsername(authenticationRequestDto.getLogin())
                .orElseThrow(() -> new RegistrationException(USER_NOT_FOUND.MESSAGE, USER_NOT_FOUND.ERROR_CODE));

        if (!passwordEncoder.matches(authenticationRequestDto.getPassword(), userEntity.getPassword())) {
            throw new AuthenticationException(INVALID_AUTH_ARGS.MESSAGE, INVALID_AUTH_ARGS.ERROR_CODE);
        }

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequestDto.getLogin(), authenticationRequestDto.getPassword()
            ));

            String token = jwtTokenProvider.generateToken(userEntity);
            return new TokenResponseDto(token);
        } catch (AuthenticationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "user.authentication.authentication_failed");
        }
    }

    public User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
