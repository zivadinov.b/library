package com.example.library.service.impl;

import com.example.library.dto.BookDto;
import com.example.library.entity.Book;
import com.example.library.repository.BookRepository;
import com.example.library.service.IAuthorService;
import com.example.library.service.IBookService;
import com.example.library.util.exception.ErrorMessageConstants;
import com.example.library.util.exception.InternalException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class BookService implements IBookService {

    private final BookRepository repository;
    private final AuthService authService;
    private final ModelMapper mapper = new ModelMapper();

    @Override
    public List<Book> fetchAll() {
        return repository.findAll();
    }

    @Override
    public Book getBookById(Long bookId) {
        return repository.findById(bookId).orElseThrow(() ->
                new InternalException(format(ErrorMessageConstants.NOT_FOUND.MESSAGE, bookId), ErrorMessageConstants.NOT_FOUND.ERROR_CODE));
    }

    @Override
    public Book update(BookDto bookDto, Long bookId) {
        Book book = getBookById(bookId);
        book.setName(bookDto.getName());
        book.setCode(bookDto.getCode());
        book.setAuthorId(bookDto.getAuthorId());
        book.setUpdatedBy(authService.getCurrentUser());
        return repository.save(book);
    }

    @Override
    public void delete(Long bookId) {
        repository.deleteById(bookId);
    }

    @Override
    public Book create(BookDto bookDto) {
        Book book = mapper.map(bookDto, Book.class);
        book.setCreatedBy(authService.getCurrentUser());
        return repository.save(book);
    }

}
