package com.example.library.service.impl;

import com.example.library.dto.AuthorDto;
import com.example.library.entity.Author;
import com.example.library.repository.AuthorRepository;
import com.example.library.service.IAuthorService;
import com.example.library.util.exception.ErrorMessageConstants;
import com.example.library.util.exception.InternalException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class AuthorService implements IAuthorService {

    private final AuthorRepository repository;
    private final ModelMapper mapper = new ModelMapper();

    @Override
    public List<Author> fetchAll() {
        return repository.findAll();
    }

    @Override
    public Author getAuthorById(Long authorId) {
        return repository.findById(authorId).orElseThrow(() ->
                new InternalException(format(ErrorMessageConstants.NOT_FOUND.MESSAGE, authorId), ErrorMessageConstants.NOT_FOUND.ERROR_CODE));
    }

    @Override
    public Author update(AuthorDto authorDto, Long authorId) {
        Author author = getAuthorById(authorId);
        author.setFirstName(authorDto.getFirstName());
        author.setLastName(authorDto.getLastName());
        author.setMiddleName(authorDto.getMiddleName());
        author.setCode(authorDto.getCode());

        return repository.save(author);
    }

    @Override
    public void delete(Long authorId) {
       repository.deleteById(authorId);
    }

    @Override
    public Author create(AuthorDto authorDto) {
        return repository.save(mapper.map(authorDto, Author.class));
    }

}
