package com.example.library.dto;


import lombok.Data;


@Data
public class BookDto {

    private Long id;

    private String code;

    private String name;

    private Long authorId;

}
