package com.example.library.dto;

import lombok.Data;

@Data
public class AuthorDto {

    private Long id;

    private String code;

    private String firstName;

    private String lastName;

    private String middleName;

}
