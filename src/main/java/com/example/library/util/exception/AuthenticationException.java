package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import org.springframework.http.HttpStatus;

public class AuthenticationException extends BaseException {

    public AuthenticationException(final String message, final String errorCode) {
        super(new ErrorResponse(message, errorCode, HttpStatus.UNAUTHORIZED, HttpStatus.UNAUTHORIZED.value()));
    }
}
