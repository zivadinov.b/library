package com.example.library.util.exception;

public class ErrorMessageConstants {

    public interface METHOD_ARGUMENT_NOT_VALID {
        String ERROR_CODE = "INVALID_ARGUMENT";
        String MESSAGE = "Method arguments are invalid";
    }

    public interface INTERNAL_SERVER_ERROR {
        String ERROR_CODE = "INTERNAL_SERVER_ERROR";
        String MESSAGE = "Server system error";
    }

    public interface INVALID_TOKEN {
        String ERROR_CODE = "UNAUTHORIZED";
        String MESSAGE = "Invalid Access Token";
    }

    public interface TOKEN_NOT_FOUND {
        String ERROR_CODE = "UNAUTHORIZED";
        String MESSAGE = "Token not found.";
    }

    public interface INVALID_ARGS {
        String ERROR_CODE = "INVALID_ARGS";
        String MESSAGE = "Invalid args";
    }

    public interface USER_EXIST {
        String ERROR_CODE = "USER_EXIST";
        String MESSAGE = "User already exist";
    }

    public interface USER_NOT_FOUND {
        String ERROR_CODE = "USER_NOT_FOUND";
        String MESSAGE = "User not exist";
    }

    public interface INVALID_AUTH_ARGS {
        String ERROR_CODE = "UNAUTHORIZED";
        String MESSAGE = "Invalid login or password";
    }

    public interface USER_NOT_ACTIVATED {
        String ERROR_CODE = "INTERNAL_SERVER_ERROR";
        String MESSAGE = "User not activated yet";
    }

    public interface NOT_FOUND {
        String ERROR_CODE = "NOT_FOUND";
        String MESSAGE = "Requested resource of type [%s] with identifier [%s] not found";
    }

    public interface REPOSITORY_NOT_FOUND {
        String ERROR_CODE = "REPOSITORY_NOT_FOUND";
        String MESSAGE = "Requested repository not found";
    }

    public interface REGISTRATION_ERROR {
        String ERROR_CODE = "REGISTRATION_ERROR";
        String MESSAGE = "Error occurred: %s";
    }

    public interface AUTHORIZATION_ERROR {
        String ERROR_CODE = "AUTHORIZATION_ERROR";
        String MESSAGE = "Error occurred: %s";
    }

    public interface UPDATE_ERROR {
        String ERROR_CODE = "UPDATE_ERROR";
        String MESSAGE = "Error occurred: %s";
    }

    public interface DELETE_ERROR {
        String ERROR_CODE = "DELETE_ERROR";
        String MESSAGE = "Error occurred: %s";
    }

    public interface CONTENT_NOT_FOUND {
        String ERROR_CODE = "CONTENT_NOT_FOUND";
        String MESSAGE = "Content does not find";
    }

    public interface INVALID_ARGUMENT {
        String ERROR_CODE = "INVALID_ARGUMENT";
        String MESSAGE = "Occurs an invalid parameter";
    }

    public interface DATA_DUPLICATE {
        String ERROR_CODE = "DATA_DUPLICATE";
        String MESSAGE = "Data with code [%s] already exist";
    }

    public interface BAD_REQUEST {
        String ERROR_CODE = "BAD_REQUEST";
        String MESSAGE = "Bad request";
    }

    public interface UnauthorizedError {
        String ERROR_CODE = "UNAUTHORIZED";
        String MESSAGE = "Не авторизован. Ошибка при валидации токена.";
    }
}
