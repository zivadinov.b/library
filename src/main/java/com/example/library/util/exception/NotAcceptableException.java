package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class NotAcceptableException extends BaseException {

    public NotAcceptableException(String message, String errorCode) {
        super(new ErrorResponse(message, errorCode, HttpStatus.NOT_ACCEPTABLE, HttpStatus.NOT_ACCEPTABLE.value()));
    }
}
