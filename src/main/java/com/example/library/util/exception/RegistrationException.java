package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RegistrationException extends BaseException {

    public RegistrationException(final String message, final String errorCode) {
        super(new ErrorResponse(message, errorCode, HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }
}
