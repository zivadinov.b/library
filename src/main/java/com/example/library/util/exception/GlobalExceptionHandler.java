package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ErrorResponse> handleBaseException(final BaseException e) {
        log.error(e.getErrorResponse().toString(), e);
        return ResponseEntity
                .status(e.getErrorResponse().getResponseStatus())
                .body(e.getErrorResponse());
    }

    // error handle for @Valid
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(final MethodArgumentNotValidException e,
                                                                               final WebRequest request) {
        log.error(e.getMessage(), e);
        // Get all errors
        final List<String> errors = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        final ErrorResponse errorResponse
                = new ErrorResponse(ErrorMessageConstants.METHOD_ARGUMENT_NOT_VALID.MESSAGE + errors, ErrorMessageConstants.METHOD_ARGUMENT_NOT_VALID.ERROR_CODE,
                HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), request.getDescription(false));

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
//
//    @ExceptionHandler(value = UnauthorisedException.class)
//    @ResponseStatus(HttpStatus.UNAUTHORIZED)
//    public ResponseEntity<ErrorResponse> handleUnauthorizedException(final UnauthorisedException e) {
//        log.error(e.getErrorResponse().toString(), e);
//        return new ResponseEntity<>(e.getErrorResponse(), e.getErrorResponse().getResponseStatus());
//    }


    @ExceptionHandler(value = {NotAcceptableException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    protected ResponseEntity<ErrorResponse> handleNotAcceptableException(NotAcceptableException ex,
                                                                         WebRequest request) {
        return new ResponseEntity<>(ex.getErrorResponse(), ex.getErrorResponse().getResponseStatus());
    }

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException ex,
                                                                            WebRequest request) {
        return new ResponseEntity<>(ex.getErrorResponse(), ex.getErrorResponse().getResponseStatus());
    }

    @ExceptionHandler(value = {BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ErrorResponse> handleBadRequestException(NotAcceptableException ex,  WebRequest request) {
        final String errorMsg = ex.getErrorResponse().getErrorMsg();
        final String errorCode = ex.getErrorResponse().getErrorCode();
        final int responseCode = ex.getErrorResponse().getResponseCode();
        final ErrorResponse errorResponseDto =
                ErrorResponse.builder()
                        .errorMsg(ex.getClass().getName())
                        .responseCode(responseCode)
                        .errorCode(errorCode)
                        .details(errorMsg)
                        .responseStatus(HttpStatus.BAD_REQUEST)
                        .build();
        return new ResponseEntity<>(
                errorResponseDto,
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({RegistrationException.class})
    public ResponseEntity<ErrorResponse> handleRegistrationException(final RegistrationException ex) {
        log.error(ex.getMessage(), ex);
        final String errorMsg = ex.getErrorResponse().getErrorMsg();
        final String errorCode = ex.getErrorResponse().getErrorCode();
        final int responseCode = ex.getErrorResponse().getResponseCode();
        final ErrorResponse errorResponseDto =
                ErrorResponse.builder()
                        .errorMsg(ex.getClass().getName())
                        .responseCode(responseCode)
                        .errorCode(errorCode)
                        .details(errorMsg)
                        .responseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                        .build();
        return new ResponseEntity<>(
                errorResponseDto,
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({UnauthorisedException.class})
    public ResponseEntity<ErrorResponse> handleAuthenticationException(final UnauthorisedException ex) {
        log.error(ex.getMessage(), ex);

        final String errorMsg = ex.getErrorResponse().getErrorMsg();
        final String errorCode = ex.getErrorResponse().getErrorCode();
        final int responseCode = ex.getErrorResponse().getResponseCode();

        final ErrorResponse errorResponseDto =
                ErrorResponse.builder()
                        .errorMsg(ex.getClass().getName())
                        .responseCode(responseCode)
                        .errorCode(errorCode)
                        .details(errorMsg)
                        .responseStatus(HttpStatus.UNAUTHORIZED)
                        .build();
        return new ResponseEntity<>(
                errorResponseDto,
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<ErrorResponse> handleMissingServletRequestParameterException(final MissingServletRequestParameterException ex) {
        log.error(ex.getMessage(), ex);

        final ErrorResponse errorResponseDto =
                ErrorResponse.builder()
                        .errorMsg(ex.getClass().getName())
                        .details(ex.getMessage())
                        .responseStatus(HttpStatus.BAD_REQUEST)
                        .build();
        return new ResponseEntity<>(
                errorResponseDto,
                HttpStatus.BAD_REQUEST);
    }

    // handle all other exceptions.
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleAllExceptions(final Exception e, final WebRequest request) {
        log.error(e.getMessage() + " : " + request.getDescription(true), e);

        final ErrorResponse errorResponse
                = new ErrorResponse(ErrorMessageConstants.INTERNAL_SERVER_ERROR.MESSAGE, ErrorMessageConstants.INTERNAL_SERVER_ERROR.ERROR_CODE,
                HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(),
                request.getDescription(false));

        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
