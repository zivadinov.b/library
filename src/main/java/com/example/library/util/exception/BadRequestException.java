package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BadRequestException extends BaseException {

    public BadRequestException(final String message, final String errorCode) {
        super(new ErrorResponse(message, errorCode, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value()));
    }
}
