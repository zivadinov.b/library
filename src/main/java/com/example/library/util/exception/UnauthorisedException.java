package com.example.library.util.exception;


import com.example.library.util.model.ErrorResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class UnauthorisedException extends BaseException {

    public UnauthorisedException(final String message, final String errorCode) {
        super(new ErrorResponse(message, errorCode, HttpStatus.UNAUTHORIZED, HttpStatus.UNAUTHORIZED.value()));
    }

}
