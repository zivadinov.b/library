package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class BaseException extends RuntimeException {
    private ErrorResponse errorResponse;

}
