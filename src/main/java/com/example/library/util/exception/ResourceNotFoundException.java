package com.example.library.util.exception;

import com.example.library.util.model.ErrorResponse;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

@Getter
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseException {

    public ResourceNotFoundException(final Class<?> type, final Object identifier) {
        super(new ErrorResponse(format(ErrorMessageConstants.NOT_FOUND.MESSAGE, type.getSimpleName(), identifier),
                ErrorMessageConstants.NOT_FOUND.ERROR_CODE, HttpStatus.NO_CONTENT, HttpStatus.NO_CONTENT.value()));

    }
}
