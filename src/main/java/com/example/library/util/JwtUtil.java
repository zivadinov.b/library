package com.example.library.util;

import com.example.library.entity.Session;
import com.example.library.entity.User;
import com.example.library.repository.SessionRepository;
import com.example.library.util.exception.JwtAuthenticationException;
import com.example.library.util.model.ValidateTokenResponse;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {

    @Value("${security.secret-key}")
    private String SECRET_KEY;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private UserDetailsService userDetailsService;

    public String extractUsername(String token) throws Exception {
        return extractClaim(token, Claims::getSubject);
    }

    public ValidateTokenResponse validate(String token) throws Exception {
        ValidateTokenResponse validateTokenResponse = new ValidateTokenResponse();
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parse(token);

            Session session = sessionRepository.getByToken(token);
            validateTokenResponse.setValid(true);
            validateTokenResponse.setActive(session.getActive());
            validateTokenResponse.setExpiredDate(session.getExpDate());
            return validateTokenResponse;
        } catch (Exception e) {
            validateTokenResponse.setValid(false);
            return validateTokenResponse;
        }
    }

    public Date extractExpiration(String token) throws Exception {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) throws Exception {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    public Boolean isTokenExpired(String token) throws Exception {
        return extractExpiration(token).before(new Date());
    }

    public String getUsernameFromToken(String token) {
        return extractAllClaims(token).getSubject();
    }


    public String generateToken(User userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("user", userDetails);
        return createToken(claims, userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) throws Exception {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public boolean validateToken(String token) throws JwtAuthenticationException {
        try {
            return !extractAllClaims(token).getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is invalid", HttpStatus.UNAUTHORIZED);
        }
    }

    public String resolveToken(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }


    public Authentication getAuthentication(String token) {

        String username = getUsernameFromToken(token);
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }
}
