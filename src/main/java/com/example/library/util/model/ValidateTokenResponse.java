package com.example.library.util.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ValidateTokenResponse {

    private Boolean valid;
    private Boolean active;
    private Timestamp expiredDate;

}
