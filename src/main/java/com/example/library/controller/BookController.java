package com.example.library.controller;

import com.example.library.dto.BookDto;
import com.example.library.dto.ResponseDto;
import com.example.library.entity.Book;
import com.example.library.service.IBookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/books", produces = APPLICATION_JSON_VALUE)
@Tag(name = "Book", description = "Book management resource")
public class BookController {

    private final IBookService bookService;

    @GetMapping
    @Operation(summary = "Get all books")
    public ResponseEntity<List<Book>> fetchBooks(){
        return new ResponseEntity<>(bookService.fetchAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get book by id")
    public ResponseEntity<Book> getByBookId(@PathVariable Long id){
        return new ResponseEntity<>(bookService.getBookById(id), HttpStatus.OK);
    }

    @PostMapping
    @Operation(summary = "Create new book", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Book> addBook(@Valid @RequestBody BookDto bookDto){
        try {
            return new ResponseEntity<>(bookService.create(bookDto), HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Update book info", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Book> updateBook(@Valid @RequestBody BookDto bookDto, @PathVariable Long id) {
        try {
            return new ResponseEntity<>(bookService.update(bookDto, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete book by id", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<ResponseDto> deleteBookById(@PathVariable Long id){
        try {
            bookService.delete(id);
            return new ResponseEntity<>(ResponseDto.builder().value("Book deleted successfully").build(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
