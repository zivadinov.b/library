package com.example.library.controller;

import com.example.library.service.impl.AuthService;
import com.example.library.util.model.AuthenticationRequest;
import com.example.library.util.model.TokenResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Tag(name = "AUTH", description = "AUTH management resource")
public class LoginController {

    private final AuthService authService;

    @PostMapping("/login")
    @Operation(summary = "Authentication by username and password")
    public ResponseEntity<TokenResponseDto> login(@Valid @RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authService.authenticate(request));
    }
}
