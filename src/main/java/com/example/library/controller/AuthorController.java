package com.example.library.controller;

import com.example.library.dto.AuthorDto;
import com.example.library.dto.ResponseDto;
import com.example.library.entity.Author;
import com.example.library.service.IAuthorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/authors", produces = APPLICATION_JSON_VALUE)
@Tag(name = "Author", description = "Author management resource")
public class AuthorController {

    private final IAuthorService authorService;

    @GetMapping
    @Operation(summary = "Get all author")
    public ResponseEntity<List<Author>> fetchBooks(){
        return new ResponseEntity<>(authorService.fetchAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get author by id")
    public ResponseEntity<Author> getByBookId(@PathVariable Long id){
        return new ResponseEntity<>(authorService.getAuthorById(id), HttpStatus.OK);
    }

    @PostMapping
    @Operation(summary = "create new author", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Author> addBook(@Valid @RequestBody AuthorDto authorDto){
        try {
            return new ResponseEntity<>(authorService.create(authorDto), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Change author's cred", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Author> updateBook(@Valid @RequestBody AuthorDto authorDto, @PathVariable Long id) {
        try {
            return new ResponseEntity<>(authorService.update(authorDto, id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete author", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<ResponseDto> deleteBookById(@PathVariable Long id){
        try {
            authorService.delete(id);
            return new ResponseEntity<>(ResponseDto.builder().value("Author deleted successfully").build(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
