package com.example.library.mapper;

import com.example.library.dto.AuthorDto;
import com.example.library.entity.Author;
import org.mapstruct.Mapper;

@Mapper
public interface AuthorMapper {

    Author dtoToEntity(AuthorDto dto);
    AuthorDto entityToDto(Author entity);
}
