package com.example.library.entity;


import jdk.jfr.Timestamp;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@Data
@Table(name = "book")
public class Book {

    @Id
    @SequenceGenerator(name = "BOOK_ID_SEQUENCE_GENERATOR", schema = "public", sequenceName = "book_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BOOK_ID_SEQUENCE_GENERATOR")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "author_id")
    private Long authorId;

    @OneToOne
    @JoinColumn(name="author_id", referencedColumnName="id", insertable = false, updatable = false)
    private Author author;

    @Column(name = "created_date")
    @Timestamp
    @CreationTimestamp
    private Instant createDate;

    @Column(name = "updated_date")
    @Timestamp
    @UpdateTimestamp
    private Instant updatedDate;

    @JoinColumn(name="updated_by", referencedColumnName="id")
    @OneToOne
    private User updatedBy;

    @JoinColumn(name="created_by", referencedColumnName="id")
    @OneToOne
    private User createdBy;

}
