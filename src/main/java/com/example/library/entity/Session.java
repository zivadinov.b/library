package com.example.library.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "session")
public class Session {
    @Id
    private Long id;

    @Column
    private Long userId;

    @Column
    private Timestamp dat;
    @Column
    private String token;
    @Column
    private Timestamp expDate;
    @Column
    private Boolean active;

    public Session() {
    }
}
