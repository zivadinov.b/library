package com.example.library.config;

import com.example.library.util.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerAdviceConfig extends GlobalExceptionHandler {

}
