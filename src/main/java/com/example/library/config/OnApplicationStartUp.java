package com.example.library.config;

import com.example.library.entity.Role;
import com.example.library.entity.User;
import com.example.library.repository.RoleRepository;
import com.example.library.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Slf4j
@Component
@RequiredArgsConstructor
public class OnApplicationStartUp {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("OnApplicationStartUp.onApplicationEvent");
        //Write your business logic here.
        if (userRepository.findAll().size() <= 0) {
            preloadData();
        }else{
            log.info("No need to add users to db");
        }
    }
    private void preloadData() {
        Role role;
        if (roleRepository.findAll().size() <= 0) {
            role = new Role();
            role.setName("ADMIN");
            role = roleRepository.save(role);
        } else {
            role = roleRepository.findAll().get(0);
        }

        User user = new User();
        user.setUsername("username");
        user.setPassword(passwordEncoder.encode("123"));
        user.setLastName("Admin");
        user.setFirstName("Admin");
        user.setMiddleName("Admin");
        user.setEmail("info@library.kz");
        user.setAuthorities(Collections.singletonList(role));
        user.setAccountExpired(false);
        user.setAccountLocked(false);
        user.setEnabled(true);
        user.setCredentialsExpired(false);
        user = userRepository.save(user);
        log.info("User preload data:: " + user);
    }
}
