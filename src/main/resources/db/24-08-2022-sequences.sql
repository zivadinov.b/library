create sequence author_id_seq;
alter sequence author_id_seq owner to postgres;
alter sequence author_id_seq owned by author.id;

create sequence book_id_seq;
alter sequence book_id_seq owner to postgres;
alter sequence book_id_seq owned by book.id;


create sequence user_id_seq;
alter sequence user_id_seq owner to postgres;
alter sequence user_id_seq owned by "user".id;
