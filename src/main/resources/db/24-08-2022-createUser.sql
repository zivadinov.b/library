create table "user"
(
    id           bigint constraint user_pk primary key,
    username     varchar(128) constraint user_uk unique,
    password     varchar(500),
    email        varchar(256),
    first_name   varchar(256),
    last_name    varchar(256),
    middle_name  varchar(256),
    reg_date     timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    account_expired     boolean  NULL,
    account_locked      boolean  NULL,
    credentials_expired boolean  NULL,
    enabled             boolean  NOT NULL
);

alter table "user" owner to postgres;

create unique index user_id_uindex on "user" (id);

CREATE TABLE role
(
    id   serial PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    UNIQUE (name)
);

CREATE TABLE user_roles
(
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    PRIMARY KEY ( user_id, role_id ),
    FOREIGN KEY (user_id) REFERENCES "user"(id),
    FOREIGN KEY (role_id) REFERENCES role(id)
);

INSERT INTO role(name) VALUES ('ADMIN'), ('USER');
