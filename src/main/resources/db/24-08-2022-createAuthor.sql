create table author
(
    id         bigint constraint author_pk primary key,
    code         varchar(128),
    first_name   varchar(256),
    last_name    varchar(256),
    middle_name  varchar(256)
);

alter table author owner to postgres;

create unique index author_id_uindex on author (id);
