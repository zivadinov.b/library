
create table book
(
    id           bigint
        constraint book_pk
            primary key,
    code         varchar(128),
    name         varchar(256),
    author_id    integer constraint book_author_id_fk references author,
    created_date timestamp,
    updated_date timestamp,
    created_by   integer constraint book_author_id_fk_3 references "user"(id),
    updated_by   integer constraint book_author_id_fk_2 references "user"(id)
);

alter table book owner to postgres;

create unique index book_id_uindex on book (id);
